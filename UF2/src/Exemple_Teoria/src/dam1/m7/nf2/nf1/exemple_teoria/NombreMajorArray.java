/**
 * GNU General Public License <http://www.gnu.org/licenses/>
 *
 * CFGS Desenvolupament d'Aplicacions Multiplataforma Departament d'Informàtica
 * IES Eugeni d'Ors - Vilafranca del Penedès
 *
 * Curs 2016 - 2017
 *
 * Mòdul M7 - Desenvolupament d'Interfícies Pràctica:
 *
 * @author Rubén Nadal <rnadal27@xtec.cat>
 * @version 1.0
 *
 */
package dam1.m7.nf2.nf1.exemple_teoria;

/**
 *
 * @author Rubén Nadal <rnadal27@xtec.cat>
 */
public class NombreMajorArray {

    /**
     * Retorna l'element major d'una llista
     *
     * @param llista Un array d'enters
     * @return El valor major de la llista
     */
    public static int nombreMajor(int llista[]) {
        int index, max = Integer.MAX_VALUE;        
        for (index = 0; index < llista.length - 1; index++) {
            if (llista[index] > max) {
                max = llista[index];
            }
        }
        return max;
    }
}
