/**
 * GNU General Public License <http://www.gnu.org/licenses/>
 *
 * CFGS Desenvolupament d'Aplicacions Multiplataforma Departament d'Informàtica
 * IES Eugeni d'Ors - Vilafranca del Penedès
 *
 * Curs 2016 - 2017
 *
 * Mòdul M7 - Desenvolupament d'Interfícies Pràctica:
 *
 * @author Rubén Nadal <rnadal27@xtec.cat>
 * @version 1.0
 *
 */
package dam1.m7.uf2.nf1.exemple_teoria;

import dam1.m7.nf2.nf1.exemple_teoria.NombreMajorArray;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Rubén Nadal <rnadal27@xtec.cat>
 */
public class NombreMajorArrayTest {

    public NombreMajorArrayTest() {
    }

    /**
     * Esquelet crear per l'assistent
     */
    @Test
    public void testNombreMajor() {
        System.out.println("nombreMajor");
        int[] llista = new int[]{3, 7, 9, 8};
        int expResult = 9;
        int result = NombreMajorArray.nombreMajor(llista);
        assertEquals(expResult, result);
        // Cal eliminar fail !!
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    @Test
    public void testSimple() {
        System.out.println("testSimple");
        assertEquals(9, NombreMajorArray.nombreMajor(new int[]{3, 7, 9, 8}));
    }

    @Test
    public void testOrdre() {
        System.out.println("testOrdre");
        assertEquals(9, NombreMajorArray.nombreMajor(new int[]{9, 7, 8}));
        assertEquals(9, NombreMajorArray.nombreMajor(new int[]{7, 9, 8}));       
        assertEquals(9, NombreMajorArray.nombreMajor(new int[]{7, 8, 9}));
    }

    @Test
    public void testDuplicats() {
        System.out.println("testDuplicats");
        assertEquals(9, NombreMajorArray.nombreMajor(new int[]{9, 7, 9, 8}));
    }
    
    @Test
    public void testSolamentUn() {
        System.out.println("testSolamentUn");
        assertEquals(7, NombreMajorArray.nombreMajor(new int[]{7}));
    }

    public void testTotsNegatius() {
        System.out.println("testTotsNegatius");
        assertEquals(-4, NombreMajorArray.nombreMajor(new int[]{-4, -6, -7, 22}));
    }

}
