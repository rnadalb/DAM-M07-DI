/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dam1.m7.uf2.nf1.carrocompra;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ruben
 */
public class ProducteTest {
    
    private Producte producte;
    
    public ProducteTest() {
    }
    
    @Before    
    public void setUp() {
        producte = new Producte("1", "Producte1", "Descripcio1", 1.0);
    }
    
    @After
    public void tearDown() throws Exception {       
        producte = null;
    }

    /**
     * Test of getCodi method, of class Producte.
     */
    @Test
    public void testGetCodi() {
        System.out.println("getCodi");        
        String expResult = "1";
        String result = producte.getCodi();
        assertEquals(expResult, result);
    }

    /**
     * Test of setCodi method, of class Producte.
     */
    @Test
    public void testSetCodi() {
        System.out.println("setCodi");
        String codi = "1";        
        producte.setCodi(codi);
    }

    /**
     * Test of getNom method, of class Producte.
     */
    @Test
    public void testGetNom() {
        System.out.println("getNom");        
        String expResult = "Producte1";
        String result = producte.getNom();
        assertEquals(expResult, result);

    }

    /**
     * Test of setNom method, of class Producte.
     */
    @Test
    public void testSetNom() {
        System.out.println("setNom");
        String nom = "Producte1";        
        producte.setNom(nom);
    }

    /**
     * Test of getDescripcio method, of class Producte.
     */
    @Test
    public void testGetDescripcio() {
        System.out.println("getDescripcio");        
        String expResult = "Descripcio1";
        String result = producte.getDescripcio();
        assertEquals(expResult, result);       
    }

    /**
     * Test of setDescripcio method, of class Producte.
     */
    @Test
    public void testSetDescripcio() {
        System.out.println("setDescripcio");
        String descripcio = "Descripcio1";
        producte.setDescripcio(descripcio);
    }

    /**
     * Test of getPreu method, of class Producte.
     */
    @Test
    public void testGetPreu() {
        System.out.println("getPreu");        
        double expResult = 1.0;
        double result = producte.getPreu();
        assertEquals(expResult, result, 0.0);     
    }

    /**
     * Test of setPreu method, of class Producte.
     */
    @Test
    public void testSetPreu() {
        System.out.println("setPreu");
        double preu = 1.0;       
        producte.setPreu(preu);        
    }
    
}
