/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dam1.m7.uf2.nf1.carrocompra;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ruben
 */
public class CarroCompraTest {
    private CarroCompra carroCompra;
    private Producte producte;
    
    public CarroCompraTest() {
    }
    
    @Before
    public void setUp() {
        carroCompra = new CarroCompra();
        producte = new Producte("1", "Producte1", "Descripcio1", 1.0);
        carroCompra.afegirProducte(producte);
    }
    
    @After
    public void tearDown() {
        carroCompra = null;
        producte = null;
    }

    /**
     * Test of getTotal method, of class CarroCompra.
     */
    @Test
    public void testGetTotal() {
        System.out.println("getTotal");
        double expResult = 1.0;
        double result = carroCompra.getTotal();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of afegirProducte method, of class CarroCompra.
     */
    @Test
    public void testAfegirProducte() {
        System.out.println("afegirProducte");
        Producte producte2 = new Producte("2", "Producte2", "Descripcio2", 2.0);        
        carroCompra.afegirProducte(producte2);
        // Test. comprovar recompte elements
        assertEquals(2, carroCompra.getRecompte());
        // Test. Comprovar total
        double totalEsperat = producte.getPreu() + producte2.getPreu();
        assertEquals(totalEsperat, carroCompra.getTotal(), 0.0);     
    }

    /**
     * Test of eliminarProducte method, of class CarroCompra.
     */
    @Test
    public void testEliminarProducte() {
        System.out.println("eliminarProducte");        
        carroCompra.eliminarProducte(producte);
        assertEquals(0, carroCompra.getRecompte(), 0.0);
    }

    /**
     * Test of getRecompte method, of class CarroCompra.
     */
    @Test
    public void testGetRecompte() {
        System.out.println("getRecompte");        
        int expResult = 1;
        int result = carroCompra.getRecompte();
        assertEquals(expResult, result);        
    }

    /**
     * Test of buida method, of class CarroCompra.
     */
    @Test
    public void testBuida() {
        System.out.println("buida");        
        carroCompra.buida();        
        assertEquals(0, carroCompra.getRecompte(), 0.0);
    }
    
}
