/**
 * GNU General Public License <http://www.gnu.org/licenses/>
 *
 * CFGS Desenvolupament d'Aplicacions Multiplataforma Departament d'Informàtica
 * IES Eugeni d'Ors - Vilafranca del Penedès
 *
 * Curs 2016 - 2017
 *
 * Mòdul M7 - Desenvolupament d'Interfícies Pràctica:
 *
 * @author Rubén Nadal <rnadal27@xtec.cat>
 * @version 1.0
 *
 */
package dam1.m7.uf2.nf1.carrocompra;

import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author Rubén Nadal <rnadal27@xtec.cat>
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({dam1.m7.uf2.nf1.carrocompra.CarroCompraTest.class, dam1.m7.uf2.nf1.carrocompra.ProducteTest.class})
public class CarroCompraSuite {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
    
}
