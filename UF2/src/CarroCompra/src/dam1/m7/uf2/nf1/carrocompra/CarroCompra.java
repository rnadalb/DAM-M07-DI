/**
 * GNU General Public License <http://www.gnu.org/licenses/>
 *
 * CFGS Desenvolupament d'Aplicacions Multiplataforma Departament d'Informàtica
 * IES Eugeni d'Ors - Vilafranca del Penedès
 *
 * Curs 2016 - 2017
 *
 * Mòdul M7 - Desenvolupament d'Interfícies Pràctica:
 *
 * @author Rubén Nadal <rnadal27@xtec.cat>
 * @version 1.0
 *
 */
package dam1.m7.uf2.nf1.carrocompra;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author Rubén Nadal <rnadal27@xtec.cat>
 */
public class CarroCompra {

    private ArrayList llitaSeleccionats;

    public CarroCompra() {
        llitaSeleccionats = new ArrayList();
    }
    
    public double getTotal() {
        double total = 0.00;
        for (Iterator i = llitaSeleccionats.iterator(); i.hasNext();) {
            Producte item = (Producte) i.next();
            total += item.getPreu();
        }
        return total;
    }

    public void afegirProducte(Producte item) {
        llitaSeleccionats.add(item);
    }

    public void eliminarProducte(Producte item) {
        llitaSeleccionats.remove(item);
    }

    public int getRecompte() {
        return llitaSeleccionats.size();
    }

    public void buida() {
        llitaSeleccionats.clear();
    }
}
