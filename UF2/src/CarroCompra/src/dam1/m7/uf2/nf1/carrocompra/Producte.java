/**
 * GNU General Public License <http://www.gnu.org/licenses/>
 *
 * CFGS Desenvolupament d'Aplicacions Multiplataforma Departament d'Informàtica
 * IES Eugeni d'Ors - Vilafranca del Penedès
 *
 * Curs 2016 - 2017
 *
 * Mòdul M7 - Desenvolupament d'Interfícies Pràctica:
 *
 * @author Rubén Nadal <rnadal27@xtec.cat>
 * @version 1.0
 *
 */
package dam1.m7.uf2.nf1.carrocompra;

/**
 *
 * @author Rubén Nadal <rnadal27@xtec.cat>
 */
public class Producte {
    
    private String codi;
    private String nom;
    private String descripcio;
    private double preu;

    // Constructor
    
    public Producte(String codi, String nom, String descripcio, double preu) {
        this.codi = codi;
        this.nom = nom;
        this.descripcio = descripcio;
        this.preu = preu;
    }

    // Getter's & Setter's \\
    public String getCodi() {
        return codi;
    }

    public void setCodi(String codi) {
        this.codi = codi;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescripcio() {
        return descripcio;
    }

    public void setDescripcio(String descripcio) {
        this.descripcio = descripcio;
    }

    public double getPreu() {
        return preu;
    }

    public void setPreu(double preu) {
        this.preu = preu;
    }
    
    
}
