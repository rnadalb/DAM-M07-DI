/**
 * GNU General Public License <http://www.gnu.org/licenses/>
 *
 * CFGS Desenvolupament d'Aplicacions Multiplataforma Departament d'Informàtica
 * IES Eugeni d'Ors - Vilafranca del Penedès
 *
 * Curs 2016 - 2017
 *
 * Mòdul M7 - Desenvolupament d'Interfícies Pràctica:
 *
 * @author Rubén Nadal <rnadal27@xtec.cat>
 * @version 1.0
 *
 */
package dam1.m7.uf2.nf1.calculadora;

/**
 *
 * @author Rubén Nadal <rnadal27@xtec.cat>
 */
public class Calculadora {

    public double suma(double nombre1, double nombre2) {
        return nombre1 + nombre2;
    }

    public double resta(double nombre1, double nombre2) {
        return nombre1 - nombre2;
    }

    public double multiplica(double nombre1, double nombre2) {
        return nombre1 * nombre2;
    }

    public double divideix(double nombre1, double nombre2) {
        return nombre1 / nombre2;

    }
}
