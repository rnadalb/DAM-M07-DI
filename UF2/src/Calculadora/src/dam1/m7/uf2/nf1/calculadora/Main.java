/**
 * GNU General Public License <http://www.gnu.org/licenses/>
 *
 * CFGS Desenvolupament d'Aplicacions Multiplataforma Departament d'Informàtica
 * IES Eugeni d'Ors - Vilafranca del Penedès
 *
 * Curs 2016 - 2017
 *
 * Mòdul M7 - Desenvolupament d'Interfícies Pràctica:
 *
 * @author Rubén Nadal <rnadal27@xtec.cat>
 * @version 1.0
 *
 */
package dam1.m7.uf2.nf1.calculadora;

/**
 *
 * @author Rubén Nadal <rnadal27@xtec.cat>
 */
public class Main {

    public static void main(String[] args) {
        Calculadora calc = new Calculadora();
        
        double a = 3, b = 2;
        double res = calc.suma(a, b);
        
        System.out.println("El resultat és: " + res);
    }
}