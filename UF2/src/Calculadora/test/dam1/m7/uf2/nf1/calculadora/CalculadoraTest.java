/**
 * GNU General Public License <http://www.gnu.org/licenses/>
 *
 * CFGS Desenvolupament d'Aplicacions Multiplataforma Departament d'Informàtica
 * IES Eugeni d'Ors - Vilafranca del Penedès
 *
 * Curs 2016 - 2017
 *
 * Mòdul M7 - Desenvolupament d'Interfícies Pràctica:
 *
 * @author Rubén Nadal <rnadal27@xtec.cat>
 * @version 1.0
 *
 */
package dam1.m7.uf2.nf1.calculadora;

import dam1.m7.uf2.nf1.calculadora.Calculadora;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Rubén Nadal <rnadal27@xtec.cat>
 */
public class CalculadoraTest {

    public CalculadoraTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of suma method, of class Calculadora.
     */
    @Test
    public void testSuma() {
        System.out.println("suma");
        double nombre1 = 3.0;
        double nombre2 = 2.0;
        Calculadora instance = new Calculadora();
        double expResult = 5.0;
        double result = instance.suma(nombre1, nombre2);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of resta method, of class Calculadora.
     */
    @Test
    public void testResta() {
        System.out.println("resta");
        double nombre1 = 5.0;
        double nombre2 = 2.0;
        Calculadora instance = new Calculadora();
        double expResult = 3.0;
        double result = instance.resta(nombre1, nombre2);
        assertEquals(expResult, result, 0.0);
        // Zero
        assertEquals(instance.resta(5, 5), 0, 0.0);
        // Valor negatiu
        assertEquals(instance.resta(3, 5), -2, 0.0);
        // 1 negatiu - 1 positiu
        assertEquals(instance.resta(-3, 5), -8, 0.0);
        // 1 negatiu - 1 negatiu
        assertEquals(instance.resta(-3, -5), 2, 0.0);
    }

    /**
     * Test of multiplica method, of class Calculadora.
     */
    @Test
    public void testMultiplica() {
        System.out.println("multiplica");
        double nombre1 = 2.0;
        double nombre2 = 2.0;
        Calculadora instance = new Calculadora();
        double expResult = 4.0;
        double result = instance.multiplica(nombre1, nombre2);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of divideix method, of class Calculadora.
     */
    @Test
    public void testDivideix() {
        System.out.println("divideix");
        double nombre1 = 0.0;
        double nombre2 = 0.0;
        Calculadora instance = new Calculadora();
        double expResult = 0.0;
        double result = instance.divideix(nombre1, nombre2);
        assertEquals(expResult, result, 0.0);
    }
}
