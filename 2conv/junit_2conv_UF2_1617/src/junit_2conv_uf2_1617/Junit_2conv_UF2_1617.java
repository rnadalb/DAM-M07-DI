/**
 * GNU General Public License <http://www.gnu.org/licenses/>
 *
 * CFGS Desenvolupament d'Aplicacions Multiplataforma Departament d'Informàtica
 * IES Eugeni d'Ors - Vilafranca del Penedès
 *
 * Curs 2016 - 2017
 *
 * Mòdul M7 - Desenvolupament d'Interfícies Pràctica:
 *
 * @author Rubén Nadal <rnadal27@xtec.cat>
 * @version 1.0
 *
 */
package junit_2conv_uf2_1617;

/**
 *
 * @author Rubén Nadal <rnadal27@xtec.cat>
 */
public class Junit_2conv_UF2_1617 {

    /**
     * @param args arguments de la línia d'ordres
     * @throws junit_2conv_uf2_1617.ProducteNotFoundException
     */
    public static void main(String[] args) throws ProducteNotFoundException {
        Producte p1 = new Producte("P1", 1);
        Producte p2 = new Producte("P2", 2);
        Producte p3 = new Producte("P3", 3);
        Producte p4 = new Producte("P4", 4);
        
        
        CarroCompra carro = new CarroCompra();
        
        carro.afegirProducte(p1);
        carro.afegirProducte(p2);
        carro.afegirProducte(p3);
        carro.afegirProducte(p4);
        
        System.out.println("Inserim 4 productes\n" + carro.toString());
        
        carro.eliminaProducte(p2);
        
        System.out.println("Eliminem producte 2\n" + carro.toString());
        
        System.out.println("Obtenim el total\n" + carro.getTotal() + "€");
        
        carro.buida();
        
        System.out.println("Buidem el carro\n" + carro.toString());
        System.out.println("Obtenim el recompte\n" + carro.getRecompte());
  
    }
    
}
