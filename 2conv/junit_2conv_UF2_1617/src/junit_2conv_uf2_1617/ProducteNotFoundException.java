/**
 * GNU General Public License <http://www.gnu.org/licenses/>
 *
 * CFGS Desenvolupament d'Aplicacions Multiplataforma Departament d'Informàtica
 * IES Eugeni d'Ors - Vilafranca del Penedès
 *
 * Curs 2016 - 2017
 *
 * Mòdul M7 - Desenvolupament d'Interfícies Pràctica:
 *
 * @author Rubén Nadal <rnadal27@xtec.cat>
 * @version 1.0
 *
 */
package junit_2conv_uf2_1617;


/**
 * Excepció llançada si un producte no es troba al carro de la compra
 */
public class ProducteNotFoundException extends Exception {

    /**
     * Constructor
     */
    public ProducteNotFoundException() {
        super();
    }
}
