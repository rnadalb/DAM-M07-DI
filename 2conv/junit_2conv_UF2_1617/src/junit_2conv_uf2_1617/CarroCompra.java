/**
 * GNU General Public License <http://www.gnu.org/licenses/>
 *
 * CFGS Desenvolupament d'Aplicacions Multiplataforma Departament d'Informàtica
 * IES Eugeni d'Ors - Vilafranca del Penedès
 *
 * Curs 2016 - 2017
 *
 * Mòdul M7 - Desenvolupament d'Interfícies Pràctica:
 *
 * @author Rubén Nadal <rnadal27@xtec.cat>
 * @version 1.0
 *
 */
package junit_2conv_uf2_1617;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author Rubén Nadal <rnadal27@xtec.cat>
 */
public class CarroCompra {

    private ArrayList _items;

    /**
     * Construcor
     */
    public CarroCompra() {
        _items = new ArrayList();
    }

    /**
     * Retorna el total
     *
     * @return total.
     */
    public double getTotal() {
        Iterator i = _items.iterator();
        double balance = 0.00;
        while (i.hasNext()) {
            Producte p = (Producte) i.next();
            balance = balance + p.getPreu();
        }

        return balance;
    }

    /**
     * Insereix un nou producte
     *
     * @param p Producte.
     */
    public void afegirProducte(Producte p) {
        _items.add(p);
    }

    /**
     * Elimina el producte especificat
     *
     * @param p Producte.
     * @throws ProducteNotFoundException si el producte no existeix
     */
    public void eliminaProducte(Producte p) throws ProducteNotFoundException {
        if (!_items.remove(p)) {
            throw new ProducteNotFoundException();
        }
    }

    /**
     * Retorna el nombre d'elements que conté el carro
     *
     * @return Recompte d'elements.
     */
    public int getRecompte() {
        return _items.size();
    }

    /**
     * Buida del carret.
     */
    public void buida() {
        _items = new ArrayList();
    }

    /**
     * Ens indica si el carret es buit
     *
     * @return true si el carret està buit
     */
    public boolean isBuit() {
        return (_items.size() == 0);
    }

    /**
     * 
     * @return Descripció del contingut del carro
     */
    @Override
    public String toString() {
        Iterator i = _items.iterator();
        StringBuilder sb = new StringBuilder();
        while (i.hasNext()) {
            Producte p = (Producte) i.next();
            sb.append(p.getNom()).append(" [").append(p.getPreu()).append("]").append("\n");            
        }
        return sb.toString();
    }

}
