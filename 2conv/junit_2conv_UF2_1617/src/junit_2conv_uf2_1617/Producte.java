/**
 * GNU General Public License <http://www.gnu.org/licenses/>
 *
 * CFGS Desenvolupament d'Aplicacions Multiplataforma Departament d'Informàtica
 * IES Eugeni d'Ors - Vilafranca del Penedès
 *
 * Curs 2016 - 2017
 *
 * Mòdul M7 - Desenvolupament d'Interfícies Pràctica:
 *
 * @author Rubén Nadal <rnadal27@xtec.cat>
 * @version 1.0
 *
 */
package junit_2conv_uf2_1617;

/**
 *
 * @author Rubén Nadal <rnadal27@xtec.cat>
 */
public class Producte {

    private String _nom;
    private double _preu;

    /**
     * Constructor
     *
     * @param nom Nom del producte
     * @param preu Preu del producte
     */
    public Producte(String nom, double preu) {
        _nom = nom;
        _preu = preu;
    }

    /**
     * Returna el nom del producte
     *
     * @return Nom.
     */
    public String getNom() {
        return _nom;
    }

    /**
     * Retorna el preu del producte
     *
     * @return Preu.
     */
    public double getPreu() {
        return _preu;
    }

    /**
     * Comprova si dos productes són iguals
     *
     * @param o Producte a comparar
     * @return true si els productes són iguals
     */
    @Override
    public boolean equals(Object o) {

        if (o instanceof Producte) {
            Producte p = (Producte) o;
            return p.getNom().equals(_nom);
        }

        return false;
    }
}
