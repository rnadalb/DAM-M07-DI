NF 2: Usabilitat
===
- [Introducció a la Usabilitat](./DAM1_M07_UF1.NF1_Te-03-Introduccio_Usabilitat.pdf)
- [Regles de disseny d'interfícies](./DAM1_M07_UF1.NF1_Te-04-regles_disseny_interficies.pdf)
- [Disseny Centrat en l'Usuari](./DAM1_M07_UF1.NF1_Te-05-disseny_centrat_usuari.pdf)
