/**
 * GNU General Public License <http://www.gnu.org/licenses/>
 *
 * CFGS Desenvolupament d'Aplicacions Multiplataforma Departament d'Informàtica
 * IES Eugeni d'Ors - Vilafranca del Penedès
 *
 * Curs 2016 - 2017
 *
 * Mòdul M7 - Desenvolupament d'Interfícies Pràctica:
 *
 * @author Rubén Nadal <rnadal27@xtec.cat>
 * @version 1.0
 *
 */
package dam1.m7.uf1.repasuf1;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.swing.JRViewer;

/**
 *
 * @author Rubén Nadal <rnadal27@xtec.cat>
 */
public class ImprimirInforme extends JFrame {
    
    public void mostraInforme(String informe, Map parametres) {
        try {
            // RNB: Compilem en runtime. (fitxer.jrxml)
            JasperReport report = JasperCompileManager.compileReport(getClass().getResourceAsStream("informes/" + informe));
            // RNB. Ja compilat (fitxer.jasper)
            //JasperReport informe = (JasperReport) JRLoader.loadObjectFromFile(getClass().getResource("informes/report1.jasper").toString());
            JasperPrint printer = JasperFillManager.fillReport(report, parametres, SQLHelper.getInstancia().getConnection());            
            
            JRViewer visor = new JRViewer(printer);

            visor.setOpaque(true);
            visor.setVisible(true);

            this.add(visor);
            this.setSize(640, 480);

            this.setVisible(true);
           
        } catch (JRException ex) {
            Logger.getLogger(ImprimirInforme.class.getName()).log(Level.SEVERE, null, ex);
        }
    }   
}