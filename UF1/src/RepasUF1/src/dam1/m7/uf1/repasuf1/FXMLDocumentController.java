/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dam1.m7.uf1.repasuf1;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;

/**
 *
 * @author ruben
 */
public class FXMLDocumentController {
    
    @FXML
    private TextField tfValor;

    
    @FXML
    protected void onMouseClicked(MouseEvent event) {
        if (event.getButton() == MouseButton.PRIMARY) {
            if (!tfValor.getText().isEmpty()) {
                    System.out.println("clicked");
                    Map parametres = new HashMap();
                    parametres.put("where", Integer.parseInt(tfValor.getText()));
                    ImprimirInforme informe = new ImprimirInforme();
                    informe.mostraInforme("report1.jrxml", parametres);    
            }
        }
    }
    
    @FXML
    protected void onKeyTyped(KeyEvent event) {        
        if (!event.getCharacter().matches("[0-9]")) {            
            event.consume();
        }
    }
    
}
