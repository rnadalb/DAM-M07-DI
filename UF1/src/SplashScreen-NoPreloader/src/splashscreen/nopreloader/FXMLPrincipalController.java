/**
 * CFGS Desenvolupament d'Aplicacions Multiplataforma
 * Departament d'Informàtica
 * IES Eugeni d'Ors - Vilafranca del Penedès
 * 
 * Curs 2016 - 2017
 * 
 * Mòdul M7 - Desenvolupament d'Interfícies
 * Pràctica: 5
 * 
 * @author Rubén Nadal (rnadal27@xtec.cat)
 * @version 1.0
 * 
 */
package splashscreen.nopreloader;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

public class FXMLPrincipalController implements Initializable {
    
    @FXML
    private Label label;
    
    @FXML
    private void handleButtonAction(ActionEvent event) {
        System.out.println("You clicked me!");
        label.setText("Hello World!");
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
