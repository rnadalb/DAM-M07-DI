/**
 * CFGS Desenvolupament d'Aplicacions Multiplataforma
 * Departament d'Informàtica
 * IES Eugeni d'Ors - Vilafranca del Penedès
 * 
 * Curs 2016 - 2017
 * 
 * Mòdul M7 - Desenvolupament d'Interfícies
 * Exemple: Splash Screen sense preloader
 * 
 * @author Rubén Nadal (rnadal27@xtec.cat)
 * @version 1.0
 * 
 */
package splashscreen.nopreloader;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class SplashScreenNoPreloader extends Application {
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("SplashScreenFXML.fxml"));        
        Scene scene = new Scene(root);
        stage.initStyle(StageStyle.UNDECORATED);
        stage.setScene(scene);     
        stage.show();
    }
    
    public static void main(String[] args) {
        launch(args);
    }
    
}
