/*
 * Copyright (C) 2016 fdisk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package splashscreen.nopreloader;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class SplashScreenController implements Initializable {

    @FXML
    private AnchorPane rootPane;
    
    private final long SLEEP = 3000;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        new SplashScreen().start();
    }

    // Creen una classe interna (inner class) --> Una classe dins d'una altra
    // Aquesta la definim com un nou Thread d'execució
    class SplashScreen extends Thread {
        // Tota classe que esten de la classe Thread ha de sobreescriure, al menys, el mètode run()
        @Override
        public void run() {
            try {
                // Esperem SLEEP milisegons ...
                Thread.sleep(SLEEP);
            
                // Executa un Runnable en una aplicació JavaFX en un temps no especificat i en el futur.
                // L'objecte s'insereix a una cua d'esdeveniments i retornarà el control, una vegada executat, 
                // immediatament a qui l'ha cridat
                // Els objectes Runnable s'execurtaran en l'ordre de crida
                
                // Les expressions Lambda són una novetat de Java 8.0
                // Si volem utilitzar una expressió Lambda, la signatura del següent mètode seria
                //      Platform.runLater(() -> {
                // Per ser una mica més senzill d'entedre ho deixem en el format anterior
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        Parent root = null;
                        try {
                            root = FXMLLoader.load(getClass().getResource("FXMLPrincipal.fxml"));
                        } catch (IOException ex) {
                            Logger.getLogger(SplashScreenController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        Scene scene = new Scene(root);
                        Stage stage = new Stage();
                        stage.setScene(scene);                        
                        stage.show();
                        rootPane.getScene().getWindow().hide();    
                    }                    
                });
            } catch (InterruptedException ex) {
                Logger.getLogger(SplashScreenController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }    
    }
}