/**
 * CFGS Desenvolupament d'Aplicacions Multiplataforma
 * Departament d'Informàtica
 * IES Eugeni d'Ors - Vilafranca del Penedès
 *
 * Curs 2016 - 2017
 *
 * Mòdul M7 - Desenvolupament d'Interfícies
 * Pràctica: 5
 *
 * @author Rubén Nadal (rnadal27@xtec.cat)
 * @version 1.0
 *
 */
package dam1.m7.uf1.tresenratlla;

import javafx.animation.RotateTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.GridPane;
import javafx.util.Duration;

public class TresEnRatllaController {

    @FXML Button b1;
    @FXML Button b2;
    @FXML Button b3;
    @FXML Button b4;
    @FXML Button b5;
    @FXML Button b6;
    @FXML Button b7;
    @FXML Button b8;
    @FXML Button b9;
    
    @FXML MenuItem mnuSobreApp;

    @FXML GridPane tauler;

    private boolean primerJugador = true;
    private final String X = "X";
    private final String O = "O";
    private final double ANIM_DURADA = 800;
    private final int ANIM_REPETICIONS = 2;
    
    @FXML
    public void botoClickAction(ActionEvent event) {

        Button botoPolsat = (Button) event.getTarget();
        String labelBoto = botoPolsat.getText();

        if (labelBoto.isEmpty() && primerJugador) {
            botoPolsat.setText(X);
            primerJugador = false;
        } else if (labelBoto.isEmpty() && !primerJugador) {
            botoPolsat.setText(O);
            primerJugador = true;
        }
    
        winner();
    }
    
    private boolean winner() {
        if (comprovaFiles()) return true;
        if (comprovaColumnes()) return true;
        if (comprovaDiagonals()) return true;
        
        return false;
    }

    private boolean comprovaFiles() {
        //Fila 1
        if (!b1.getText().isEmpty()           &&
            b1.getText().equals(b2.getText()) &&
            b2.getText().equals(b3.getText())) {
            
                mostraCombinacioGuanyadora(b1, b2, b3);
                return true;
        }
        
        //Fila 2
        if (!b4.getText().isEmpty()           &&
            b4.getText().equals(b5.getText()) &&
            b5.getText().equals(b6.getText())) {
            
                mostraCombinacioGuanyadora(b4, b5, b6);
                return true;
        }

        //Fila 3
        if (!b7.getText().isEmpty()           &&
            b7.getText().equals(b8.getText()) &&
            b8.getText().equals(b9.getText())) {
            
            mostraCombinacioGuanyadora(b7, b8, b9);
            return true;
        }
        
        return false;
    }
    
    private boolean comprovaColumnes() {
        //Columna 1
        if (!b1.getText().isEmpty()            &&
             b1.getText().equals(b4.getText()) &&
             b4.getText().equals(b7.getText())) {
            
            mostraCombinacioGuanyadora(b1, b4, b7);
            return true;
        }
        
        //Columna 2
        if (!b2.getText().isEmpty()            &&
             b2.getText().equals(b5.getText()) &&
             b5.getText().equals(b8.getText())) {
            
            mostraCombinacioGuanyadora(b2, b5, b8);
            return true;
        }
        //Columna 3
        if (!b3.getText().isEmpty()            &&
             b3.getText().equals(b6.getText()) &&
             b6.getText().equals(b9.getText())) {
            
            mostraCombinacioGuanyadora(b3, b6, b9);
            return true;
        }
        return false;
    }

    private boolean comprovaDiagonals() {
        //Diagonal 1
        if (!b1.getText().isEmpty()            &&
             b1.getText().equals(b5.getText()) &&
             b5.getText().equals(b9.getText())) {
            
            mostraCombinacioGuanyadora(b1, b5, b9);
            return true;
        }
        
        //Diagonal 2
        if (!b3.getText().isEmpty()            &&
             b3.getText().equals(b5.getText()) &&
             b5.getText().equals(b7.getText())) {
            
            mostraCombinacioGuanyadora(b3, b5, b7);
            return true;
        }
        return false;
    }

    private void mostraCombinacioGuanyadora(Button primer, Button segon, Button tercer) {
        primer.getStyleClass().add("guanyador");
        segon.getStyleClass().add("guanyador");
        tercer.getStyleClass().add("guanyador");
        animacio(ANIM_DURADA, ANIM_REPETICIONS, tercer);
    }
    
    private void animacio(double durada, int repeticions, Node node) {
        RotateTransition rt1 = new RotateTransition(Duration.millis(durada), node);        
        rt1.setToAngle(180);
        rt1.setFromAngle(0);
        rt1.setAutoReverse(true);
        rt1.setCycleCount(repeticions);
        rt1.play();
    }
    
}