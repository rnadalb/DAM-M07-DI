/**
 * CFGS Desenvolupament d'Aplicacions Multiplataforma
 * Departament d'Informàtica
 * IES Eugeni d'Ors - Vilafranca del Penedès
 * 
 * Curs 2016 - 2017
 * 
 * Mòdul M7 - Desenvolupament d'Interfícies
 * Pràctica: 5
 * 
 * @author Rubén Nadal (rnadal27@xtec.cat)
 * @version 1.0
 * 
 */
package dam1.m7.uf1.tresenratlla;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class TresEnRatlla extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root;
        root = FXMLLoader.load(getClass().getResource("view/TresEnRatlla.fxml"));
        
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.show();
    }
    
    public static void main(String[] args) {
        launch(args);
    }
    
}
