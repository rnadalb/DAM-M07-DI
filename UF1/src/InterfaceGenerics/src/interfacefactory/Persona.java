package interfacefactory;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Persona {
    
    private final StringProperty nom;
    private final StringProperty cognoms;    

    public Persona() {
        this("", "");
    }

    public Persona(String nom, String cognoms) {        
        this.nom = new SimpleStringProperty(nom);
        this.cognoms = new SimpleStringProperty(cognoms);                    
    }

    public String getNom() {
        return nom.get();
    }

    public void setNom(String firstName) {
        this.nom.set(firstName);
    }

    public StringProperty nomProperty() {
        return nom;
    }

    public String getCognoms() {
        return cognoms.get();
    }

    public void setCognoms(String lastName) {
        this.cognoms.set(lastName);
    }

    public StringProperty cognomsProperty() {
        return cognoms;
    }
}