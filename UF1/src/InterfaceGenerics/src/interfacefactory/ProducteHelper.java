/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfacefactory;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author ruben
 */
public class ProducteHelper implements InterfaceGenerics<Producte>{

    private final ArrayList<Producte> llista;
    
    public ProducteHelper() {
        this.llista = new ArrayList<Producte>();
    }
    
    @Override
    public void insereix(Producte item) {
        llista.add(item);
    }

    @Override
    public void elimina(int id) {
        llista.remove(id);
    }

    @Override
    public void print() {        
        Iterator<Producte> it = llista.iterator();
        while(it.hasNext()) {
            Producte p = it.next();
            System.out.println(p.getNom() + " " + p.getMarca());
        }    
    }
    
}
