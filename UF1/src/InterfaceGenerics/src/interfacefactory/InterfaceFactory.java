/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfacefactory;

/**
 *
 * @author ruben
 */
public class InterfaceFactory {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Persona p1 = new Persona("Felipe 1", "Torrent");
        Persona p2 = new Persona("Felipe 2", "Torrent");
        Persona p3 = new Persona("Felipe 3", "Torrent");
        Persona p4 = new Persona("Felipe 4", "Torrent");
        
        Producte prod1 = new Producte("Producte 1", "Marca");
        Producte prod2 = new Producte("Producte 2", "Marca");
        Producte prod3 = new Producte("Producte 3", "Marca");
        Producte prod4 = new Producte("Producte 4", "Marca");
        
        PersonaHelper helperPersona = new PersonaHelper();
        helperPersona.insereix(p1);
        helperPersona.insereix(p2);
        helperPersona.insereix(p3);
        helperPersona.insereix(p4);
        
        helperPersona.print();
        
        ProducteHelper helperProducte = new ProducteHelper();
        helperProducte.insereix(prod1);
        helperProducte.insereix(prod2);
        helperProducte.insereix(prod3);
        helperProducte.insereix(prod4);
        
        helperProducte.print();
        
        
    }
    
}
