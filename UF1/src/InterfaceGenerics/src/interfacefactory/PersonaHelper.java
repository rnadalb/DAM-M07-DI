/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfacefactory;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author ruben
 */
public class PersonaHelper implements InterfaceGenerics<Persona> {
    private final ArrayList<Persona> llista;
    
    public PersonaHelper() {
        this.llista = new ArrayList<Persona>();
    }
    
    @Override
    public void insereix(Persona item) {
        llista.add(item);
    }

    @Override
    public void elimina(int id) {
        llista.remove(id);
    }

    @Override
    public void print() {        
        Iterator<Persona> it = llista.iterator();
        while(it.hasNext()) {
            Persona p = it.next();
            System.out.println(p.getNom() + " " + p.getCognoms());
        }    
    }
}
