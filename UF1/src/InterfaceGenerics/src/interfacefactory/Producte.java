
package interfacefactory;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Producte {
    private final StringProperty nom;
    private final StringProperty marca;    
    
    public Producte() {
        this ("", "");
    };
    
    public Producte(String nom, String marca) {
        this.nom = new SimpleStringProperty(nom);        
        this.marca = new SimpleStringProperty(marca);        
    }    

    public String getNom() {
        return nom.get();
    }

    public void setNom(String nom) {
        this.nom.set(nom);
    }

    public StringProperty nomProperty() {
        return nom;
    }
    
    public String getMarca() {
        return marca.get();
    }

    public void setMarca(String marca) {
        this.marca.set(marca);
    }

    public StringProperty marcaProperty() {
        return marca;
    }   
}
