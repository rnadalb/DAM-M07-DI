package interfacefactory;

public interface InterfaceGenerics<T> {
    public void insereix (T item);    
    public void elimina (int id);    
    public void print();    
}