package dam1.m7.uf1.menusfxml;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;

public class FXMLPrincipalController implements Initializable {
    
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }
    
    @FXML
    private void mnuSurt(final ActionEvent event) {
        Alert alert = new Alert(AlertType.ERROR, "Vol sortir de l'aplicació?", ButtonType.YES, ButtonType.NO);
        alert.showAndWait();

        if (alert.getResult() == ButtonType.YES) {
            Platform.exit();
        }
    }
    
    @FXML
    private void mnuSobreApp(final ActionEvent event) {
        mostraAlerta(AlertType.INFORMATION, "Aplicació desenvolupada per...");
    }
    
    private void mostraAlerta (AlertType tipus, String missatge) {
        Alert alert = new Alert(tipus, missatge, ButtonType.YES);
        alert.showAndWait();
    }
    
}
