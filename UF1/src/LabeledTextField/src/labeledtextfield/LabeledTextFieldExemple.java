/**
 * GNU General Public License <http://www.gnu.org/licenses/>
 *
 * CFGS Desenvolupament d'Aplicacions Multiplataforma Departament d'Informàtica
 * IES Eugeni d'Ors - Vilafranca del Penedès
 *
 * Curs 2016 - 2017
 *
 * Mòdul M7 - Desenvolupament d'Interfícies Pràctica:
 *
 * @author Rubén Nadal <rnadal27@xtec.cat>
 * @version 1.0
 *
 */
package labeledtextfield;

import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author Rubén Nadal <rnadal27@xtec.cat>
 */
public class LabeledTextFieldExemple extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        LabeledTextField control = new LabeledTextField();
        control.setText("Hello!");
        
        stage.setScene(new Scene(control));
        stage.setTitle("Control Personalitzat !!!");
        stage.setWidth(300);
        stage.setHeight(200);
        stage.show();
    }
    
    public static void main(String[] args) {
        launch(args);
    }
}
