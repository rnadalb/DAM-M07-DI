/**
 * GNU General Public License <http://www.gnu.org/licenses/>
 *
 * CFGS Desenvolupament d'Aplicacions Multiplataforma Departament d'Informàtica
 * IES Eugeni d'Ors - Vilafranca del Penedès
 *
 * Curs 2016 - 2017
 *
 * Mòdul M7 - Desenvolupament d'Interfícies Pràctica:
 *
 * @author Rubén Nadal <rnadal27@xtec.cat>
 * @version 1.0
 *
 */
package provacontrolpersonalitzat;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author Rubén Nadal <rnadal27@xtec.cat>
 */
public class ProvaControlPersonalitzat extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
        
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
