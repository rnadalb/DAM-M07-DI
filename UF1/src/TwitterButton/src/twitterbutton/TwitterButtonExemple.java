/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package twitterbutton;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author ruben
 */
public class TwitterButtonExemple extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        TwitterButton control = new TwitterButton();
        
        stage.setScene(new Scene(control));
        stage.setTitle("Control Personalitzat !!!");
        stage.setWidth(300);
        stage.setHeight(200);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
    
}
