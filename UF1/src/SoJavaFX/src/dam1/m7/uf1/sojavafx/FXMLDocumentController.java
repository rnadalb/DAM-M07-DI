/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dam1.m7.uf1.sojavafx;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Slider;
import javafx.scene.media.AudioClip;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

public class FXMLDocumentController implements Initializable {
    
    @FXML private Button btnTic, btnTac, btnWinner;
    @FXML private RadioButton rbAudioClip;
    @FXML private Slider slVolum;
    @FXML private Label lbVolum;
              
    @FXML
    private void btnAction(ActionEvent event) {
        URL recurs = null;
       
        if (event.getTarget().equals(btnTic)) {
            recurs = getClass().getResource("recursos/tic.wav");
        } else if (event.getTarget().equals(btnTac)) {
            recurs = getClass().getResource("recursos/tac.wav");
        } else if (event.getTarget().equals(btnWinner)) {
            recurs = getClass().getResource("recursos/winner.wav");
        } else {
            Platform.exit();
        }
        
        if (rbAudioClip.isSelected()) {
            AudioClip clip;
            clip = new AudioClip(recurs.toString());
        
            clip.play(slVolum.getValue());
            
            System.out.println("AudioClip: " + recurs.toString());
        } else {
            Media media = new Media(recurs.toString());
            MediaPlayer mp = new MediaPlayer(media);
            
            mp.setVolume(slVolum.getValue());
            mp.play();
            
            System.out.println("MediaPlayer: " + recurs.toString());
        }

        System.out.println(event.getTarget().toString());
        
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // Ajustar el valor del slider al label
        slVolum.valueProperty().addListener((observable, oldValue, newValue) -> {
            Double valor = newValue.doubleValue() * 100;
            lbVolum.setText(valor.intValue() + "%");        
        });
        
    }    
    
}
