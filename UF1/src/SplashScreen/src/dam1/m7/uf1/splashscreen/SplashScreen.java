/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dam1.m7.uf1.splashscreen;

import com.sun.javafx.application.LauncherImpl;
import javafx.application.Application;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import splashscreen.preloader.SplashScreen_Preloader;

/**
 *
 * @author fdisk
 */
public class SplashScreen extends Application {

    BooleanProperty ready = new SimpleBooleanProperty(false);

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLPrincipal.fxml"));

        Scene scene = new Scene(root);

        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments
     * @throws java.lang.Exception
     */
    public static void main(String[] args) throws Exception {
    //public void main(String[] args) throws Exception {
        //launch(args);

        // Inici normal
        LauncherImpl.launchApplication(SplashScreen.class, SplashScreen_Preloader.class, args);
    }
}
